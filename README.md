# A Local Dev Environment for React, Lambda, and DynamoDB

Cloud development is definitely the future. However, if you have to deploy something everytime you want to make a change, it can cut down on productivity. It would be awesome to have all 3 tiers running locally so you can code and test away without deploying or impacting other team members.

To get started:
```
git clone git@gitlab.com:ezlotnick95/local-react-lambda-dynamo-dev-environment.git
cd local-react-lambda-dynamo-dev-environment
mkdir dynamodb
docker compose up
```
Wait a few minutes for all the docker layers to download. The lambda will run on port 4000. The React web server is on port 3000. Point your browser to http://localhost:3000 and see this very lame starting point. Make a change in the code at `frontend/src/App.js` and watch the page change before your eyes.

