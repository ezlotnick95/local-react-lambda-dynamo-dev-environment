import { useState, useEffect } from 'react';
import { Navbar, Table } from 'react-bootstrap';
import axios from 'axios';

function App() {
  const [fruits, setFruits] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const results = await axios.get('http://localhost:4000/dev/fruit');
      setFruits(results.data.fruits);
    }
    fetchData();
  }, []);

  if (fruits.length === 0) {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>AWESOME DEMO</Navbar.Brand>
        </Navbar>
        <h1>Loading...</h1>
      </div>
    )
  }

  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>AWESOME DEMO</Navbar.Brand>
      </Navbar>
      <Table className="ml-3 mt-3" striped bordered>
        <thead>
          <tr>
            <th>name</th>
            <th>price</th>
            <th>description</th>
          </tr>
        </thead>
        <tbody>
        {fruits.map((fruit, index) => {
          return (
            <tr key={index}>
              <td>{fruit.name}</td>
              <td>${fruit.price}</td>
              <td>{fruit.description}</td>
            </tr>
          )
        })}
        </tbody>
      </Table>
    </div>
  );
}

export default App;
