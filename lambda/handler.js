const aws = require('aws-sdk');
if (process.env.IS_OFFLINE == 'true') {
  aws.config.update({
    region: 'us-west-2',
    endpoint: 'http://dynamo:8000'
  });
} else {
  aws.config.update({
    region: 'us-west-2'
  });
}

module.exports.fruit = async (event) => {
  const docClient = new aws.DynamoDB.DocumentClient();
  try {
    const fruits = await docClient.scan({
      TableName: 'Fruit'
    }).promise();
    return {
      statusCode: 200,
      body: JSON.stringify({fruits: fruits.Items})
    }
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify(e)
    }
  }
};
