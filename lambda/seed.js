const aws = require('aws-sdk');
aws.config.update({
    region: 'us-west-2',
    endpoint: 'http://dynamo:8000'
});
const main = async () => {
    const dynamodb = new aws.DynamoDB();
    const docClient = new aws.DynamoDB.DocumentClient();
    try {
        const existingTables = await dynamodb.listTables({}).promise();
        if (existingTables.TableNames.indexOf('Fruit') === -1) {
            const params = {
                AttributeDefinitions: [
                    {
                        AttributeName: 'name',
                        AttributeType: 'S'
                    }
                ],
                KeySchema: [
                    {
                        AttributeName: 'name',
                        KeyType: 'HASH'
                    }
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                },
                TableName: 'Fruit'
            };
            await dynamodb.createTable(params).promise();
        }
        const items = [
            {
                name: 'apple',
                price: .99,
                description: 'Honeycrisp'
            },
            {
                name: 'banana',
                price: .79,
                description: 'Ripe'
            },
            {
                name: 'orange',
                price: 1.88,
                description: 'Sunshine'
            }
        ];
        const insertions = items.map((item) => {
            return docClient.put({
                TableName: 'Fruit',
                Item: item
            }).promise();
        })
        await Promise.all(insertions);
    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}

main();