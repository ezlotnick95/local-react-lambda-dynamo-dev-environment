#!/bin/bash
set -e

echo "Creating and seeding table(s)..."
until node seed.js
do
    echo "Dynamo may not be up... Waiting..."
    sleep 3s
done
sls offline start